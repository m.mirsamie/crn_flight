<?php

class source_class {

    public $my;
    public $connected = FALSE;
    public $data = array();

    public function __construct() {
        $conf = new conf();
        $this->my = new mysqli($conf->local_host, $conf->local_user, $conf->local_pass, $conf->local_db);
        if ($this->my->connect_errno !== FALSE) {
            $this->my->set_charset("utf8");
            $this->connected = TRUE;
        }
    }

    public function loadByCustomer($customer_id) {
        $out = array();
        $customer_id = (int) $customer_id;
        if ($this->connected) {
            $res = $this->my->query("select source_id,is_all from customer_source where customer_id = $customer_id");
            while ($r = $res->fetch_assoc()) {
                $out[] = array("id" => (int) $r['source_id'], "is_all" => ((int) $r['is_all'] == 1));
            }
        }
        return($out);
    }

    public function loadGoharAgency($customer_id) {
        $out = array();
        $customer_id = (int) $customer_id;
        if ($this->connected) {
            $res = $this->my->query("select agency_id from customer_gohar where en = 1 and customer_id = $customer_id");
            while ($r = $res->fetch_assoc()) {
                $out[] = (int) $r['agency_id'];
            }
        }
        return($out);
    }

    public function loadArbabiAirline($customer_id) {
        $out = array();
        $customer_id = (int) $customer_id;
        if ($this->connected) {
            $res = $this->my->query("select airline.id,airline.name from arbabi_airline left join airline on (airline.id=airline_id) where en = 1 and customer_id = $customer_id");
            while ($r = $res->fetch_assoc()) {
                $out[] = array("id" => (int) $r['id'], "name" => $r['name']);
            }
        }
        return($out);
    }

    public function createWhere($customer_id) {
        $s = $this->loadByCustomer($customer_id);
        $wer = " and 1 = 0";
        if (count($s) > 0) {
            $ss = array();
            $limited_source = array();
            foreach ($s as $st) {
                if (!$st['is_all']) {
                    $limited_source[] = $st['id'];
                } else {
                    $ss[] = $st['id'];
                }
            }
            if (count($ss) > 0) {
                $wer = " and ((flight.source_id in (" . implode(",", $ss) . "))";
            } else {
                $wer = '';
            }
            $lim = array();
            foreach ($limited_source as $ls) {
                if ($ls == 1) {
                    $ag = $this->loadGoharAgency($customer_id);
                    if (count($ag) > 0) {
                        $lim[] = "flight.source_id = $ls and flight.agency_id in (" . implode(",", $ag) . ")";
                    }
                } else if ($ls == 2) {
                    $ar = $this->loadArbabiAirline($customer_id);
                    $g = array();
                    foreach ($ar as $arr) {
                        $g[] = $arr['name'];
                    }
                    if (count($g) > 0) {
                        $lim[] = "flight.source_id = $ls and flight_extra.airline in ('" . implode("','", $g) . "')";
                    }
                }
            }
            if (count($lim) > 0) {
                $wer .= (($wer == '')?' and (':' or ')." (" . implode(") or (", $lim) . "))";
            }
            else
            {
                $wer .= ")";
            }
            //$wer = var_export($lim,TRUE);
        }
        return($wer);
    }

}
