<?php

class flight_class {

    public $my;
    public $connected = FALSE;
    public $data = array();
    public $query = '';

    public function __construct($load, $param,$wer='') {
        $conf = new conf();
        $this->my = new mysqli($conf->local_host, $conf->local_user, $conf->local_pass, $conf->local_db);
        if ($this->my->connect_errno !== FALSE) {
            $this->my->set_charset("utf8");
            $this->connected = TRUE;
            if ($load === TRUE) {
                $werc = array();
                foreach ($param as $key => $value) {
                    if ($key != 'aztarikh' && $key != 'tatarikh' && $key != 'extra' && $key != 'airline' && $key != 'sort' && $key != 'way' && $key != 'from_city' && $key != 'to_city' && $key != 'limit') {
                        $werc[] = " `$key` = '$value' ";
                    } else if ($key == 'aztarikh') {
                        $werc[] = " `fdate` >= '$value' ";
                    } else if ($key == 'tatarikh') {
                        $werc[] = " `fdate` <= '$value' ";
                    } else if ($key == 'airline') {
                        $werc[] = " `airline` in ('" . implode("','", $value) . "') ";
                    } else if ($key == 'way' && trim($param['from_city'])!='' && trim($param['to_city'])!='') {
                        if ($param['way'] == 'one') {
                            $werc[] = " `from_city` = '" . $param['from_city'] . "' and `to_city` = '" . $param['to_city'] . "' and `typ` <> 2 ";
                        } else if ($param['way'] != 'one') {
                            $werc[] = "(( `from_city` = '" . $param['to_city'] . "' and `to_city` = '" . $param['from_city'] . "') or (`from_city` = '" . $param['from_city'] . "' and `to_city` = '" . $param['to_city'] . "'))";
                        }
                    }
                }
                $sort = '';
                if(!isset($param['sort']))
                {
                    $param['sort'] = 'all';
                }
                if ($param['sort'] != 'all') {
                    $sort = ' order by ' . $param['sort'];
                }
                $limit = '';
                if (isset($param['limit']) && (int) $param['limit'] > 0) {
                    $limit = " limit " . (int) $param['limit'];
                }
                $query = "select * from flight " . ((count($werc) > 0) ? " where " . implode(" and ", $werc) : "") . $wer . $sort . $limit;
                if (isset($param['extra'])) {
                    $fields = "`agency_id`, `from_city`, `to_city`, `flight_number`, `flight`.`flight_id`, `fdate`, `ftime`,`ltime`, `typ` , `capacity`, `class_ghimat`, `class`,`buy_time`";
                    //$fields_extra = "`airline`, `airplane`, `description`, `extra`, `excurrency`, `extrad`, (`price`+`extra`) `price`, `currency`, `public`, `poursant`, `day`, `add_price`, `tax`, `taxd`, `no_public`, `open_price`, `open_price_currency`, `open_price`,`agency_site`,`bfid`,`target_capa`,`tell_time`";
                    $fields_extra = "`airline`, `airplane`, `description`, `extrad`, (`price`+`extra`) `price`, `currency`,`agency_site`,`tell_time`";
                    $query = "select `flight`.`id`,`flight`.`source_id`,$fields,$fields_extra from `flight` left join `flight_extra` on (`flight`.`id`=`flight_extra`.`flight_id`) left join `airline` on (`airline`.`name` = `flight_extra`.`airline`) where flight.en = 1  " . ((count($werc) > 0) ? ' and ' . implode(" and ", $werc) . ' ' : '') . $wer . $sort . $limit;
                }
                $this->query = $query;
                $res = $this->my->query($query);
                while ($r = $res->fetch_assoc()) {
                    $r['price'] = (int) ((int) $r['price'] / 10);
                    $this->data[] = $r;
                }
            }
        }
    }

    public function add($agency_id, $agency_name, $agency_site, $in_data, $exec = TRUE) {
        $final_query = '';
        $query = array();
        $query_extra = array();
        $fields = "(`agency_id`,`agency_name`,`agency_site`, `from_city`, `to_city`, `flight_number`, `flight_id`, `fdate`, `ftime`,`ltime`, `typ` , `capacity`, `class_ghimat`, `class`,`buy_time`)";
        $fields_extra = "(`airline`, `airplane`, `description`, `extra`, `excurrency`, `extrad`, `price`, `currency`, `public`, `poursant`, `day`, `add_price`, `tax`, `taxd`, `no_public`, `open_price`, `open_price_currency`,`bfid`,`target_capa`,`tell_time`,`flight_id`)";
        if ($this->connected) {
            $this->my->set_charset("utf8");
            for ($i = 0; $i < count($in_data); $i++) {
                $dat = $in_data[$i];
                if ((int) $dat['capacity1'] > 0) {
                    $query[] = "($agency_id,'$agency_name','$agency_site','" . $dat['from_city'] . "','" . $dat['to_city'] . "','" . $dat['flight_number'] . "','" . $dat['flight_id'] . "','" . $dat['fdate'] . "','" . $dat['ftime'] . "','" . $dat['ltime'] . "','" . $dat['type'] . "','" . $dat['capacity1'] . "','" . $dat['class1'] . "','1','" . $dat['buy_time'] . "')";
                    $query_extra[] = "('" . $dat['airline'] . "','" . $dat['airplane'] . "','" . $dat['description'] . "','" . $dat['extra'] . "','" . $dat['excurrency'] . "','" . $dat['extrad'] . "','" . $dat['price'] . "','" . $dat['currency'] . "','" . $dat['public'] . "','" . $dat['poursant1'] . "','" . $dat['day1'] . "','" . $dat['add_price1'] . "'"
                            . ",'" . $dat['tax1'] . "','" . $dat['taxd1'] . "','" . max(array($dat['best'], $dat['good'], $dat['weak'], $dat['bad'])) . "','" . $dat['open_price'] . "','" . $dat['open_currency'] . "'," . $dat['bfid'] . "," . $dat['target_capa1'] . "," . $dat['tell_time'];
                }
                if ((int) $dat['capacity2'] > 0) {
                    $query[] = "($agency_id,'$agency_name','$agency_site','" . $dat['from_city'] . "','" . $dat['to_city'] . "','" . $dat['flight_number'] . "','" . $dat['flight_id'] . "','" . $dat['fdate'] . "','" . $dat['ftime'] . "','" . $dat['ltime'] . "','" . $dat['type'] . "','" . $dat['capacity2'] . "','" . $dat['class2'] . "','2','" . $dat['buy_time'] . "')";
                    $query_extra[] = "('" . $dat['airline'] . "','" . $dat['airplane'] . "','" . $dat['description'] . "','" . $dat['extra2'] . "','" . $dat['excurrency2'] . "','" . $dat['extrad'] . "','" . $dat['price2'] . "','" . $dat['currency2'] . "','" . $dat['public2'] . "','" . $dat['poursant2'] . "','" . $dat['day2'] . "','" . $dat['add_price2'] . "'"
                            . ",'" . $dat['tax2'] . "','" . $dat['taxd2'] . "','" . max(array($dat['best2'], $dat['good2'], $dat['weak2'], $dat['bad2'])) . "','" . $dat['open_price2'] . "','" . $dat['open_currency2'] . "'," . $dat['bfid2'] . "," . $dat['target_capa2']. "," . $dat['tell_time'];
                }
                if ((int) $dat['capacity3'] > 0) {
                    $query[] = "($agency_id,'$agency_name','$agency_site','" . $dat['from_city'] . "','" . $dat['to_city'] . "','" . $dat['flight_number'] . "','" . $dat['flight_id'] . "','" . $dat['fdate'] . "','" . $dat['ftime'] . "','" . $dat['ltime'] . "','" . $dat['type'] . "','" . $dat['capacity3'] . "','" . $dat['class3'] . "','3','" . $dat['buy_time'] . "')";
                    $query_extra[] = "('" . $dat['airline'] . "','" . $dat['airplane'] . "','" . $dat['description'] . "','" . $dat['extra3'] . "','" . $dat['excurrency3'] . "','" . $dat['extrad'] . "','" . $dat['price3'] . "','" . $dat['currency3'] . "','" . $dat['public3'] . "','" . $dat['poursant3'] . "','" . $dat['day3'] . "','" . $dat['add_price3'] . "'"
                            . ",'" . $dat['tax3'] . "','" . $dat['taxd3'] . "','" . max(array($dat['best3'], $dat['good3'], $dat['weak3'], $dat['bad3'])) . "','0','0'," . $dat['bfid3'] . "," . $dat['target_capa3']. "," . $dat['tell_time'];
                }
                if ((int) $dat['capacity5'] > 0) {
                    $query[] = "($agency_id,'$agency_name','$agency_site','" . $dat['from_city'] . "','" . $dat['to_city'] . "','" . $dat['flight_number'] . "','" . $dat['flight_id'] . "','" . $dat['fdate'] . "','" . $dat['ftime'] . "','" . $dat['ltime'] . "','" . $dat['type'] . "','" . $dat['capacity5'] . "','" . $dat['class5'] . "','5','" . $dat['buy_time'] . "')";
                    $query_extra[] = "('" . $dat['airline'] . "','" . $dat['airplane'] . "','" . $dat['description'] . "','0','0','" . $dat['extrad'] . "','" . $dat['price5'] . "','" . $dat['currency5'] . "','" . $dat['public5'] . "','" . $dat['poursant5'] . "','" . $dat['day5'] . "','" . $dat['add_price5'] . "'"
                            . ",'0','0','" . max(array($dat['best5'], $dat['good5'], $dat['weak5'], $dat['bad5'])) . "','0','0'," . $dat['bfid5'] . ",0," . $dat['tell_time'];
                }
            }
        }
        $final_query = '';
        $final_extra_query = '';
        if (count($query) > 0) {
            if ($exec) {
                $qtmp = array();
                $qetmp = array();
                for ($i = 0; $i < count($query); $i++) {
                    $qtmp[] = $query[$i];
                    $qetmp[] = $query_extra[$i];
                    /*
                      if(count($qtmp)==10 || $i==count($query)-1)
                      {
                      $final_query = 'insert into `flight` '.$fields.' values '.implode(" , ", $qtmp);
                      $final_extra_query = 'insert into `flight_extra` '.$fields_extra.' values '.  implode(" , ", $qetmp);
                      $this->my->query($final_query);
                      $this->my->query($final_extra_query);
                      }
                     * 
                     */
                    $final_query = 'insert into `flight` ' . $fields . ' values ' . $qtmp[$i];
                    $this->my->query($final_query);
                    $final_extra_query = 'insert into `flight_extra` ' . $fields_extra . ' values ' . $qetmp[$i] . ',' . $this->my->insert_id . ')';
                    $this->my->query($final_extra_query);
                }
            }
            $final_query = 'insert into `flight` ' . $fields . ' values ' . implode(" , ", $query);
            $final_extra_query = 'insert into `flight_extra` ' . $fields_extra . ' values ' . implode(" , ", $query_extra);
        }
        return(array($final_query, $final_extra_query));
    }

}