<?php

class gohar_db_class {

    public $my;
    public $connected = FALSE;
    public $database = '';
    public $data = array();
    public $outData = array();

    public function __construct($aztarikh, $tatarikh, $adata = array()) {
        $conf = new conf();
        $database = isset($adata['name_db']) ? $adata['name_db'] : '';
        $host = $conf->host;
        $user = $conf->user;
        $pass = $conf->pass;
        if ($adata['id_gohar'] < 0) {
            $host = $adata['server_db'];
            $user = $adata['user_db'];
            $pass = $adata['pass_db'];
        }
        $database = trim($database);
        $aztarikh = $this->standardizePDate($aztarikh);
        $tatarikh = $this->standardizePDate($tatarikh);
        if ($database != '') {
            $this->database = $database;
            $this->my = new mysqli($host, $user, $pass, $database);
            if ($this->my->connect_errno !== FALSE) {
                $this->my->set_charset("utf8");
                $fil = $this->my->query("SELECT * from `tbl_flight` limit 1");
                $fields = array();
                if ($fil) {
                    $this->connected = TRUE;
                    while ($fields_arr = $fil->fetch_field()) {
                        $fields[] = $fields_arr->orgname;
                    }
                    $repeat_feilds_dummy = array(
                        "type", "price", "currency",
                        "public", "best", "good", "weak", "bad", "extra", "excurrency",
                        "open_price", "open_currency", "open_price", "open_currency", "bfid"
                    );
                    $repeat_feilds = array(
                        "capacity", "tax", "taxd", "class", "poursant", "day", "add_price",
                        "soto", "target_capa"
                    );
                    $j = 1;
                    $i = ($j == 1) ? '' : $j;
                    $qtmp = '';
                    while ($j < 6) {
                        $i = ($j == 1) ? '' : $j;
                        foreach ($repeat_feilds as $rf) {
                            if (in_array($rf . $j, $fields)) {
                                $qtmp .=(($qtmp != '') ? ',' : '') . "`$rf" . "$j`";
                            }
                        }
                        foreach ($repeat_feilds_dummy as $rf) {
                            if (in_array($rf . $i, $fields)) {
                                $qtmp .=(($qtmp != '') ? ',' : '') . "`$rf" . "$i`";
                            }
                        }
                        $j++;
                    }
                    $query = "SELECT `tell_time`,`city_src`.`sh_name` `from_city` , `city_des`.`sh_name` `to_city` ,`f_number` `flight_number`,`tbl_flight`.`id` `flight_id`,`date` `fdate`, `time` `ftime`,`d_time` `ltime`,`buy_time`"
                            . ",`airline`,`airplane`"
                            . ",`description` `description`,`extrad` `extrad`,"
                            . $qtmp
                            . " FROM `tbl_flight` left join `tbl_city` `city_src` on (`tbl_flight`.`source`=`city_src`.`city_id`) left join `tbl_city` `city_des` on (`tbl_flight`.`destination`=`city_des`.`city_id`)  "
                            . "WHERE `tbl_flight`.`status`='1' and (`capacity1`> '0'|| `capacity2`> '0'|| `capacity3`> '0' || `capacity5`> '0' ) and (`date` >= '$aztarikh' and `date` <='$tatarikh')";
                    $result = $this->my->query($query);
                    if ($result) {
                        while ($r = $result->fetch_array()) {
                            $this->data[] = $r;
                        }
                    }
                }
            }
        }
    }

    public function standardizePDate($inp) {
        $out = $inp;
        $tmp = explode('-', trim($inp));
        if (count($tmp) == 3) {
            $y = $tmp[0];
            $m = $tmp[1];
            $d = $tmp[2];
            if (strlen($y) < 4) {
                if ((int) $y < (int) $d) {
                    $t = $d;
                    $d = $y;
                    $y = $t;
                }
                if (strlen($y) == 2) {
                    $y = '13' . $y;
                }
            }
            if (strlen($d) == 1) {
                $d = '0' . $d;
            } else if (strlen($d) > 2 || strlen($d) == 0) {
                $d = '01';
            }
            if (strlen($m) == 1) {
                $m = '0' . $m;
            } else if (strlen($m) > 2 || strlen($m) == 0) {
                $m = '01';
            }
            $out = $y . '-' . $m . '-' . $d;
        }
        return($out);
    }

}
