<?php

error_reporting(E_ALL);
ini_set('display_errors', 1);
date_default_timezone_set("Asia/Tehran");
include './class/conf.php';
include './class/gohar_dbs_class.php';
include './class/gohar_db_class.php';
include './class/translate_class.php';
include './class/flight_class.php';
include './class/jdf.php';

function perToEnNums($inNum) {
    $outp = $inNum;
    $outp = str_replace('۰', '0', $outp);
    $outp = str_replace('۱', '1', $outp);
    $outp = str_replace('۲', '2', $outp);
    $outp = str_replace('۳', '3', $outp);
    $outp = str_replace('۴', '4', $outp);
    $outp = str_replace('۵', '5', $outp);
    $outp = str_replace('۶', '6', $outp);
    $outp = str_replace('۷', '7', $outp);
    $outp = str_replace('۸', '8', $outp);
    $outp = str_replace('۹', '9', $outp);
    return($outp);
}

$conf = new conf;
$gaam = $conf->gaam1['day'];
$my = new mysqli($conf->local_host, $conf->local_user, $conf->local_pass, $conf->local_db);
if ($my->connect_errno !== FALSE) {
    $my->set_charset("utf8");
    $res = $my->query("select is_lock,first_time1,first_time2,first_time3 from cron_lock");
    //var_dump($res);
    $r = $res->fetch_array();
    if ((int) $r['is_lock'] == 0) {
        $first_time1 = (int) $r['first_time1'];
        $first_time2 = (int) $r['first_time2'];
        $first_time3 = (int) $r['first_time3'];

        if ($first_time1 < 1) {
            $gaam = 2;
            $first_time1++;
            $first_time2 = 0;
            $first_time3 = 0;
        } elseif ($first_time2 < 1) {
            $gaam = 7;
            $first_time2++;
            $first_time3 = 0;
        } elseif ($first_time3 == 0) {
            $gaam = 60;
            $first_time3++;
        }
        else
        {
            $gaam = 2;
            $first_time1 = 0;
            $first_time2 = 0;
            $first_time3 = 0;
        }
//            echo "$cur_time - $last_lock3 <br/>\n";
//            if(($cur_time-$last_lock3)/60>$conf->gaam3['time'])
//            {
//                $gaam = $conf->gaam3['day'];
//                $cur_time = date("Y-m-d H:i:s",$cur_time);
//                $update_query = "from_time3 = '$cur_time'";
//            }
//            else if(($cur_time-$last_lock2)/60>$conf->gaam2['time'])
//            {
//                $gaam = $conf->gaam2['day'];
//                $cur_time = date("Y-m-d H:i:s",$cur_time);
//                $update_query = "from_time2 = '$cur_time'";
//            }
//            else if(($cur_time-$last_lock1)/60>$conf->gaam1['time'])
//            {
//                $gaam = $conf->gaam1['day'];
//                $cur_time = date("Y-m-d H:i:s",$cur_time);
//                $update_query = "from_time1 = '$cur_time'";
//            }
//            if($update_query!='')
//            {
//                //echo "locking...$gaam<br/>\n"."update cron_lock set is_lock = 1 , $update_query";
//                $my->query("update cron_lock set is_lock = 1 , $update_query");
//            }
//            else
//            {
//                $my->query("update cron_lock set is_lock = 0");
//                //echo "Runing too soon...";
//                die();
//            }

        $my->query("update cron_lock set is_lock = 1 , first_time1 = $first_time1 , first_time2 = $first_time2 , first_time3 = $first_time3");
    } else {
        //echo "LOCKED";
        die();
    }
} else {
    echo "DB connection error";
    die();
}
//echo "gaam = $gaam<br/>\n";
$gaam = 15;
$gdbs = new gohar_dbs_class();
$flight = new flight_class(FALSE, array());

$today = perToEnNums(jdate("Y-m-d"));
$thatday = perToEnNums(jdate("Y-m-d", strtotime(date("Y-m-d") . " + $gaam day")));
//echo "$today - $thatday<br/>\n";
if (count($gdbs->data) > 0) {
    //$my->query("truncate table flight");
    //$my->query("delete from flight where source_id = 1");
    //$my->query("truncate table flight_extra");
    //$my->query("delete from flight_extra where source_id = 1");
    $my->query("truncate table gohar_agency");
    for ($i = 0; $i < count($gdbs->data); $i++) {
        //$i = 1;
        $adata = $gdbs->data[$i];
        $id_gohar = $adata["id_gohar"];
        $agency_name = $adata["name"];
        $agency_site = $adata["site"];
        $my->query("insert into gohar_agency (agency_id,agency_name,agency_site) values ('$id_gohar','$agency_name','$agency_site')");
        $gdb = new gohar_db_class($today, $thatday, $adata);
        if ($gdb->connected != FALSE) {
            $flight->add($id_gohar, $agency_name, $agency_site, $gdb->data, TRUE);
        }
    }
    /*

     * 
     */
}
$my1 = new mysqli($conf->local_host, $conf->local_user, $conf->local_pass, $conf->local_db);
if ($my1->connect_errno !== FALSE) {
    $my1->query("update flight set en = 3 where source_id = 1 and en = 1");
    $my1->query("update flight_extra set en = 3 where source_id = 1 and en = 1");
    $my1->query("update flight set en = 1 where source_id = 1 and en = 0");
    $my1->query("update flight_extra set en = 1 where source_id = 1 and en = 0");
    $my1->query("delete from  flight where source_id = 1 and en = 3");
    $my1->query("delete from  flight_extra where source_id = 1 and en = 3");
    $my1->query("update source_table set last_update='" . date("Y-m-d H:i:s") . "' where id = 1");
    $my1->query("update cron_lock set is_lock = 0");
}
